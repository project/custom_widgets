<?php

namespace Drupal\custom_widgets\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin implementation of the 'custom_widgets_flat_select' widget.
 *
 * @FieldWidget (
 *   id = "custom_widgets_flat_select",
 *   label = @Translation("Flat select widget"),
 *   field_types = {
 *     "entity_reference",
 *   },
 *   multiple_values = TRUE
 * )
 */
class FlatSelectWidget extends OptionsSelectWidget {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings_default = [
      'force_deepest' => FALSE,
    ];
    return $settings_default + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['force_deepest'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Force selection of deepest level'),
      '#default_value' => $this->getSetting('force_deepest'),
      '#description' => $this->t('Force users to select terms from the deepest level.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    if ($this->getSetting('force_deepest')) {
      $summary[] = $this->t('Force selection of deepest level');
    }
    else {
      $summary[] = $this->t('Do not force selection of deepest level');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // Only works on taxonomy terms.
    $target_type = $this->getFieldSettings()['target_type'];
    if ($target_type !== 'taxonomy_term') {
      return $element;
    }

    // Hide parents, rename childs.
    $options = $element['#options'];

    /** @var \Drupal\taxonomy\TermStorageInterface $storage */
    $storage = \Drupal::entityTypeManager()->getStorage($target_type);

    foreach ($options as $tid => &$option) {
      if ($term = $storage->load($tid)) {
        $has_parent = FALSE;
        $option = $term->label();
        $parent_id = $term->id();
        while ($parent = $storage->loadParents($parent_id)) {
          $has_parent = TRUE;
          $parent = reset($parent);
          $option = $parent->label() . ' >> ' . $option;
          $parent_id = $parent->id();
        }

        // Hide item if no parent is set and force deepest is set.
        if (!$has_parent && $this->getSetting('force_deepest')) {
          unset($options[$tid]);
        }
      }
    }

    $element['#options'] = $options;

    return $element;
  }

}
